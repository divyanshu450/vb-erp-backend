const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const employeeSchema = new Schema({
  empName: {
    type: String,
    required: true,
    maxlength: 100,
  },
  empId: {
    type: String,
    required: true,
  },
  empEmail: {
    type: String,
    required: true,
  },
  empDoj: {
    type: String,
    required: true,
  },
  empDepartment: {
    type: String,
    required: true,
  },
  empDesignation: {
    type: String,
    required: true,
  },
  empBand: {
    type: String,
    required: true,
  },
  empCtc: {
    type: Number,
    required: true,
  },
  empReportingManager: {
    type: String,
  },
  empGraduation: {
    type: Boolean,
    required: false,
  },
  empPostGraduation: {
    type: Boolean,
    required: false,
  },
  empPersonalEmail: {
    type: String,
    required: true,
  },
  empPhoneNumber: {
    type: String,
    required: true,
  },
  empDob: {
    type: String,
    required: true,
  },
  empAboutMe: {
    type: String,
    required: true,
  },
  empHobbies: {
    type: Array,
    required: false,
  },
  empPrimaryCapability: {
    type: Array,
    required: false,
  },
  empSkillSet: {
    type: Array,
    required: false,
  },
  empCertifications: {
    type: Array,
    required: false,
  },
});

module.exports = mongoose.model("employee", employeeSchema, "employees");
