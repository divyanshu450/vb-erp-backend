const express = require("express");
const router = express.Router();
const employeeModel = require("../models/employeeModel");

//Test
router.get("/", (req, res) => {
  res.send("Inside employee route");
});

//Create Employee (FOR ADMIN)
router.post("/createEmp", (req, res) => {
  const emp = new employeeModel();
  emp.empName = req.body.empName;
  emp.empId = req.body.empId;
  emp.empEmail = req.body.empEmail;
  emp.empDoj = req.body.empDoj;
  emp.empDepartment = req.body.empDepartment;
  emp.empDesignation = req.body.empDesignation;
  emp.empBand = req.body.empBand;
  emp.empCtc = req.body.empCtc;
  emp.empReportingManager = req.body.empReportingManager;
  emp.empGraduation = req.body.empGraduation;
  emp.empPersonalEmail = req.body.empPersonalEmail;
  emp.empPhoneNumber = req.body.empPhoneNumber;
  emp.empDob = req.body.empDob;
  emp.empAboutMe = req.body.empAboutMe;

  console.log(`req.body.empEmail is ${JSON.stringify(req.body.empEmail)}`);
  emp.save((err, doc) => {
    if (err) {
      console.log(`err is ${err}`);
      res.status(400).send(`Submit all the required fields`);
    } else {
      res.send(doc);
    }
  });
});

//Get Employee details (FOR READ ONLY)
router.get("/getEmp/:empId", (req, res) => {
  employeeModel.find({ empId: req.params.empId }, (err, empData) => {
    if (err) {
      console.log(err);
    } else {
      if (empData.length === 0) {
        res.status(404).send(`Employee details not found`);
      } else {
        res.send(empData);
      }
    }
  });
});

module.exports = router;
