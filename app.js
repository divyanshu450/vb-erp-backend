const express = require("express");
const app = express();
const port = 3000;
const mongoose = require("mongoose");
const empRoute = require("./routes/employeeRoute");

mongoose.connect("mongodb://127.0.0.1:27017/employees").then(() => {
  console.log(`mongodb connected!`);
});

app.use(express.json());
app.get("/", (req, res) => {
  res.send(`Index Route`);
});

app.use("/employee", empRoute);

app.listen(port);
